using Common.DTO.DtoForSevenLinqMethods;
using Common.DTO.Project;
using Common.DTO.Task;
using Common.Enums;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace WebAPI.IntegrationTests
{
    public class ProjectsControllerIntegrationTests:IClassFixture<CustomWebApplicationFactory<Startup>>, IDisposable
    {
        private readonly HttpClient client;
        public ProjectsControllerIntegrationTests(CustomWebApplicationFactory<Startup> factory)
        {
              client = factory.CreateClient();
        }
        public void Dispose()
        {
        }

        [Fact]
        public async Task CreateProject_ShouldWork()
        {
            var createDto = new ProjectCreateDto { Name = "New Project", Description = "New Project Description",
                                     Deadline = new DateTime(2021, 6, 30), AuthorId = 1, TeamId = 1 };

            var json = JsonConvert.SerializeObject(createDto);
            HttpContent stringContent = new StringContent(json, Encoding.UTF8, "application/json");
            var response = await client.PostAsync("api/projects", stringContent);
            var stringResponse = await response.Content.ReadAsStringAsync();

            var responseDto = JsonConvert.DeserializeObject<ProjectDto>(stringResponse);
            Assert.Equal(HttpStatusCode.Created, response.StatusCode);
            Assert.Equal(createDto.Name, responseDto.Name);
            Assert.Equal(createDto.Description, responseDto.Description);
        }
        [Theory]
        [InlineData("")]
        [InlineData("veeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeery long name for project the longest than 128 characters ")]
        public async Task CreateProject_ShouldFail_IncorrectName(string name)
        {
            var createDto = new ProjectCreateDto
            {
                Name = name,
                Description = "Project Description 1",
                Deadline = new DateTime(2021, 6, 30),
                AuthorId = 1,
                TeamId = 1
            };

            var json = JsonConvert.SerializeObject(createDto);
            HttpContent stringContent = new StringContent(json, Encoding.UTF8, "application/json");
            var response = await client.PostAsync("api/projects", stringContent);
            var stringResponse = await response.Content.ReadAsStringAsync();

            var responseDto = JsonConvert.DeserializeObject<ProjectDto>(stringResponse);
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
            Assert.NotEqual(name, responseDto.Name);
        }
        [Fact]
        public async Task AllNotFinishedTasksForEveryProject_ShouldWork()
        {
            int id = 1;
            var getAllTasksResponse = await client.GetAsync($"api/projects/all-notfinished-tasks-by-user/{id}");
            var stringTasksResponse = await getAllTasksResponse.Content.ReadAsStringAsync();
            var listTasksAndProject = JsonConvert.DeserializeObject<List<NotFinishedTasksByUserIdDto>>(stringTasksResponse);

            Assert.Equal(HttpStatusCode.OK, getAllTasksResponse.StatusCode);
            Assert.True(listTasksAndProject.All(item => item.Tasks.All(task=> task.PerformerId == id)));
            Assert.True(listTasksAndProject.All(item => item.Tasks.All(task => task.State != TaskStates.Finished)));
        }
        [Theory]
        [InlineData(1,1)]
        [InlineData(1,2)]
        [InlineData(2,2)]
        public async Task AllNotFinishedTasksForEveryProject_AddOneMoreTask_To_Project(int userId, int projectId)
        {
            var createTaskDto = new TaskCreateDto
            {
                Name = "Task Name",
                Description = "Task Description 1",
                ProjectId = projectId,
                PerformerId = userId,
            };
            var getAllBeforeAdd = await client.GetAsync($"api/projects/all-notfinished-tasks-by-user/{userId}");
            var stringResponseBeforeAdd = await getAllBeforeAdd.Content.ReadAsStringAsync();
            var listTasksAndProjectBefore = JsonConvert.DeserializeObject<List<NotFinishedTasksByUserIdDto>>(stringResponseBeforeAdd);

            await client.PostAsync("api/tasks", new StringContent(JsonConvert.SerializeObject(createTaskDto), Encoding.UTF8, "application/json"));

            var getAllAfter = await client.GetAsync($"api/projects/all-notfinished-tasks-by-user/{userId}");
            var stringResponseAfter = await getAllAfter.Content.ReadAsStringAsync();
            var listTasksAndProjectAfter = JsonConvert.DeserializeObject<List<NotFinishedTasksByUserIdDto>>(stringResponseAfter);

            var before = listTasksAndProjectBefore.FirstOrDefault(x => x.Project.Id == projectId)?.Tasks.Count() ?? 0;
            var after = listTasksAndProjectAfter.FirstOrDefault(x => x.Project.Id == projectId).Tasks.Count();

            Assert.Equal(before + 1, after);
        }
        [Theory]
        [InlineData(-1)]
        [InlineData(0)]
        [InlineData(9999)]
        public async Task AllNotFinishedTasksForEveryProject_ShouldBeEmpty_IncorrectUserId(int id)
        {
            var getAllTasksResponse = await client.GetAsync($"api/projects/all-notfinished-tasks-by-user/{id}");
            var stringTasksResponse = await getAllTasksResponse.Content.ReadAsStringAsync();
            var listTasksAndProject = JsonConvert.DeserializeObject<List<NotFinishedTasksByUserIdDto>>(stringTasksResponse);

            Assert.Empty(listTasksAndProject);
        }
    }
}
