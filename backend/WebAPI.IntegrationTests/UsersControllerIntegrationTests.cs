﻿using Common.DTO.User;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Xunit;

namespace WebAPI.IntegrationTests
{
    public class UsersControllerIntegrationTests : IClassFixture<CustomWebApplicationFactory<Startup>>, IDisposable
    {
        private readonly HttpClient client;

        public UsersControllerIntegrationTests(CustomWebApplicationFactory<Startup> factory)
        {
            client = factory.CreateClient();
        }
        public void Dispose()
        {
        }
        [Fact]
        public async Task DeleteUser_ShouldWork()
        {
            int id = 1;
            var response = await client.DeleteAsync($"api/users/{id}");

            var getAllUsersResponse = await client.GetAsync("api/users");
            var stringUsersResponse = await getAllUsersResponse.Content.ReadAsStringAsync();
            var listUsers = JsonConvert.DeserializeObject<List<UserDto>>(stringUsersResponse);

            Assert.Equal(HttpStatusCode.NoContent, response.StatusCode);
            Assert.True(listUsers.All(u=>u.Id != id));
        }
        [Theory]
        [InlineData(-1)]
        [InlineData(0)]
        [InlineData(9999)]
        public async Task DeleteUser_Should_NotFound(int id)
        {
            var response = await client.DeleteAsync($"api/users/{id}");

            Assert.Equal(HttpStatusCode.NotFound, response.StatusCode);
        }
    }
}
