﻿using Common.Enums;
using DataAccessLayer.Context;
using DataAccessLayer.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace WebAPI.IntegrationTests
{
    public static class Utilities
    {
        public static void InitializeDbForTests(ProjectDbContext db)
        {
            db.Teams.AddRange(GetSeedingTeams());
            db.Projects.AddRange(GetSeedingProjects());
            db.Tasks.AddRange(GetSeedingTasks());
            db.Users.AddRange(GetSeedingUsers());
            db.SaveChanges();
        }

        public static void ReinitializeDbForTests(ProjectDbContext db)
        {
            db.Teams.RemoveRange(db.Teams);
            db.Users.RemoveRange(db.Users);
            db.Projects.RemoveRange(db.Projects);
            db.Tasks.RemoveRange(db.Tasks);
            db.SaveChanges();
            InitializeDbForTests(db);
        }

        public static List<Team> GetSeedingTeams()
        {
            Team team1 = new Team { Id = 1, CreatedAt = new DateTime(2020, 6, 30, 23, 20, 46), Name = "Team1" };
            Team team2 = new Team { Id = 2, CreatedAt = new DateTime(2020, 6, 30, 23, 20, 46), Name = "Team2" };
            Team team3 = new Team { Id = 3, CreatedAt = new DateTime(2020, 6, 30, 23, 20, 46), Name = "Team3" };
            return new List<Team> { team1, team2, team3 };
        }
        public static List<Project> GetSeedingProjects()
        {
            Project project1 = new Project
            {
                Id = 1,
                Name = "Project1",
                Description = "Project Description 1",
                CreatedAt = new DateTime(2020, 6, 30),
                Deadline = new DateTime(2021, 6, 30),
                AuthorId = 1,
                TeamId = 1
            };
            Project project2 = new Project
            {
                Id = 2,
                Name = "Project2",
                Description = "Project Description 2",
                CreatedAt = new DateTime(2020, 6, 30),
                Deadline = new DateTime(2021, 6, 30),
                AuthorId = 2,
                TeamId = 2
            };
            Project project3 = new Project
            {
                Id = 3,
                Name = "Project3",
                Description = "Project Description 3",
                CreatedAt = new DateTime(2020, 6, 30),
                Deadline = new DateTime(2021, 6, 30),
                AuthorId = 3,
                TeamId = 3
            };
            return new List<Project> { project1, project2, project3 };
        }
        public static List<TaskEntity> GetSeedingTasks()
        {
            TaskEntity task1 = new TaskEntity
            {
                Id = 1,
                Name = "Task1",
                Description = "Task Description 1",
                CreatedAt = new DateTime(2020, 5, 30),
                FinishedAt = new DateTime(2020, 6, 30),
                State = TaskStates.Canceled,
                ProjectId = 1,
                PerformerId = 6
            };
            TaskEntity task2 = new TaskEntity
            {
                Id = 2,
                Name = "Task2",
                Description = "Task Description Description 2",
                CreatedAt = new DateTime(2020, 5, 30),
                FinishedAt = new DateTime(2020, 6, 30),
                State = TaskStates.Created,
                ProjectId = 1,
                PerformerId = 5
            };
            TaskEntity task3 = new TaskEntity
            {
                Id = 3,
                Name = "Task3",
                Description = "Task Description Description Description3",
                CreatedAt = new DateTime(2020, 5, 30),
                FinishedAt = new DateTime(2020, 6, 30),
                State = TaskStates.Finished,
                ProjectId = 1,
                PerformerId = 4
            };
            TaskEntity task4 = new TaskEntity
            {
                Id = 4,
                Name = "Task4",
                Description = "Task Description 1",
                CreatedAt = new DateTime(2020, 5, 30),
                FinishedAt = new DateTime(2020, 6, 30),
                State = TaskStates.Finished,
                ProjectId = 2,
                PerformerId = 3
            };
            TaskEntity task5 = new TaskEntity
            {
                Id = 5,
                Name = "Task5",
                Description = "Task Description Description 5",
                CreatedAt = new DateTime(2020, 5, 30),
                FinishedAt = new DateTime(2020, 6, 30),
                State = TaskStates.Started,
                ProjectId = 2,
                PerformerId = 2
            };
            TaskEntity task6 = new TaskEntity
            {
                Id = 6,
                Name = "Task6",
                Description = "Task Description Description Description 6",
                CreatedAt = new DateTime(2020, 5, 30),
                FinishedAt = new DateTime(2020, 6, 30),
                State = TaskStates.Finished,
                ProjectId = 2,
                PerformerId = 1
            };
            return new List<TaskEntity> { task1 , task2 , task3 , task4 , task5 , task6 };
        }
        public static List<User> GetSeedingUsers()
        {
            User user1 = new User()
            {
                Id = 1,
                FirstName = "FirstName1",
                LastName = "LastName1",
                Email = "some@gmail.com",
                Birthday = new DateTime(2001, 5, 30),
                RegisteredAt = new DateTime(2011, 5, 30),
                TeamId = 1
            };
            User user2 = new User()
            {
                Id = 2,
                FirstName = "FirstName2",
                LastName = "LastName2",
                Email = "some2@gmail.com",
                Birthday = new DateTime(2002, 5, 30),
                RegisteredAt = new DateTime(2012, 5, 30),
                TeamId = 1
            };
            User user3 = new User()
            {
                Id = 3,
                FirstName = "FirstName3",
                LastName = "LastName3",
                Email = "some3@gmail.com",
                Birthday = new DateTime(2003, 5, 30),
                RegisteredAt = new DateTime(2013, 5, 30),
                TeamId = 2
            };
            User user4 = new User()
            {
                Id = 4,
                FirstName = "FirstName4",
                LastName = "LastName4",
                Email = "some4@gmail.com",
                Birthday = new DateTime(2004, 5, 30),
                RegisteredAt = new DateTime(2014, 5, 30),
                TeamId = 2
            };
            User user5 = new User()
            {
                Id = 5,
                FirstName = "FirstName5",
                LastName = "LastName5",
                Email = "some5@gmail.com",
                Birthday = new DateTime(2005, 5, 30),
                RegisteredAt = new DateTime(2015, 5, 30),
                TeamId = 3
            };
            User user6 = new User()
            {
                Id = 6,
                FirstName = "FirstName6",
                LastName = "LastName6",
                Email = "some6@gmail.com",
                Birthday = new DateTime(2006, 5, 30),
                RegisteredAt = new DateTime(2016, 5, 30),
                TeamId = 3
            };
            return new List<User> { user1, user2, user3, user4, user5, user6 };
        }
    }
}

