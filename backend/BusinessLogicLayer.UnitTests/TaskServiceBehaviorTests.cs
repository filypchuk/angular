﻿using AutoMapper;
using BusinessLogicLayer.Interfaces;
using BusinessLogicLayer.Services;
using Common.DTO.Task;
using DataAccessLayer.Entities;
using DataAccessLayer.Repositories;
using DataAccessLayer.Repositories.Interfaces;
using FakeItEasy;
using System.Threading.Tasks;
using Xunit;

namespace BusinessLogicLayer.UnitTests
{
    public class TaskServiceBehaviorTests
    {
        readonly ITaskService _service;
        readonly IMapper _mapper;
        readonly IRepository<TaskEntity> _repository;
        public TaskServiceBehaviorTests()
        {
            _mapper = A.Fake<IMapper>();
            _repository = A.Fake<IRepository<TaskEntity>>();
            _service = new TaskService(_repository, _mapper);
        }
        [Fact]
        public async Task UpdateTask_MustCallOnce()
        {
            await _service.Update(new TaskDto());
            A.CallTo(() => _repository.UpdateAsync(A<TaskEntity>.Ignored)).MustHaveHappenedOnceExactly();
            A.CallTo(() => _repository.SaveAsync()).MustHaveHappenedOnceExactly();
        }
    }
}
