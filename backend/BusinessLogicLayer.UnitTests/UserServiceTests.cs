﻿using AutoMapper;
using BusinessLogicLayer.Interfaces;
using BusinessLogicLayer.MappingProfiles;
using BusinessLogicLayer.Services;
using Common.DTO.User;
using DataAccessLayer.Context;
using DataAccessLayer.Entities;
using DataAccessLayer.Repositories;
using DataAccessLayer.Repositories.Interfaces;
using System;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace BusinessLogicLayer.UnitTests
{
    public class UserServiceTests : IDisposable
    {
        readonly IUserService _service;
        readonly IMapper _mapper;
        readonly IRepository<User> _repository;
        readonly InMemoryDBContext _inMemoryDB;
        readonly ProjectDbContext _context;
        public UserServiceTests()
        {
            _mapper = MapperConfigurations();
            _inMemoryDB = new InMemoryDBContext();
            _context = _inMemoryDB.GetEmptyDbContext();
            _repository = new UserRepository(_context);
            _service = new UserService(_repository, _mapper);
        }
        public void Dispose()
        {

        }
        [Theory]
        [InlineData(1)]
        [InlineData(2)]
        [InlineData(3)]
        public async Task CreateNewUser_ToTeam(int teamId)
        {
            using (var context = _inMemoryDB.GetDbContext())
            {
                var user = new UserDto
                {
                    FirstName = Guid.NewGuid().ToString(),
                    LastName = "LastName",
                    Birthday = new DateTime(2000, 1, 1),
                    TeamId = teamId
                };
                var createdUser = await _service.Create(user);
                var team = context.Teams.Find(teamId);
                Assert.Equal(user.FirstName, createdUser.FirstName);
                Assert.NotNull(team.Users.SingleOrDefault(u => u.Id == createdUser.Id));
            }
        }
        [Fact]
        public async Task UserSortByAlphabetAndTasks_ByLengthName()
        {
            using (var context = _inMemoryDB.GetEmptyDbContext())
            {
                var user1 = new User { FirstName = "XYZ", LastName = "LastName" };
                var user2 = new User { FirstName = "ABC", LastName = "LastName" };
                var task1 = new TaskEntity { Name = "short name", PerformerId = 1 };
                var task2 = new TaskEntity { Name = "looooooong name", PerformerId = 1 };
                var task3 = new TaskEntity { Name = "name", PerformerId = 2 };

                context.Users.AddRange(user1, user2);
                context.Tasks.AddRange(task1, task2, task3);
                context.SaveChanges();

                var actual = await _service.UserByAlphabetAndTasks();

                Assert.Equal(actual.ToList()[0].User.FirstName, user2.FirstName);
                Assert.Equal(actual.ToList()[1].User.FirstName, user1.FirstName);

                Assert.True(actual.All(x => x.Tasks.FirstOrDefault().Name.Length >= x.Tasks.LastOrDefault().Name.Length));
            }
        }
        [Fact]
        public async Task UsersLastProjectAndTasks_AddLastProjectYear3000()
        {
            int userId = 1;
            using (var context = _inMemoryDB.GetDbContext())
            {
                var project = new Project { AuthorId = userId, Name = "LastProjectYear3000", CreatedAt = new DateTime(3000, 1, 1) };
                var createdProject = await context.Projects.AddAsync(project);
                var taskByProject = new TaskEntity { Name = "Task By LastProject", ProjectId = createdProject.Entity.Id };
                var theLongestTask = new TaskEntity { PerformerId = userId, Name = "The Longest Task 1880-2020", CreatedAt = new DateTime(1880, 1, 1), FinishedAt = new DateTime(2020, 1, 1) };
                context.Tasks.AddRange(taskByProject, theLongestTask);
                context.SaveChanges();
                var actual = await _service.UsersLastProjectAndTasks(userId);

                Assert.Equal(project.CreatedAt.Year, actual.LastProject.CreatedAt.Year); //last project by createAt
                Assert.Equal(1, actual.CountTasksByLastProject);
                Assert.Equal(theLongestTask.Name, actual.TheLongestByTimeTask.Name);
            }
        }

        public IMapper MapperConfigurations()
        {
            var mappingConfig = new MapperConfiguration(mc =>
            {
                mc.AddProfile(new ProjectProfile());
                mc.AddProfile(new TaskProfile());
                mc.AddProfile(new TeamProfile());
                mc.AddProfile(new UserProfile());
            });
            return mappingConfig.CreateMapper();
        }
    }
}
