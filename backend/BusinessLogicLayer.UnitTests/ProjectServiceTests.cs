using AutoMapper;
using BusinessLogicLayer.Services;
using Xunit;
using DataAccessLayer.Repositories.Interfaces;
using BusinessLogicLayer.MappingProfiles;
using DataAccessLayer.Entities;
using System.Linq;
using Common.DTO.Project;
using System;
using BusinessLogicLayer.Interfaces;
using DataAccessLayer.Context;
using DataAccessLayer.Repositories;
using Common.Enums;
using System.Threading.Tasks;

namespace BusinessLogicLayer.UnitTests
{
    public class ProjectServiceTests:IDisposable
    {
        readonly IProjectService _service;
        readonly IMapper _mapper;
        readonly IRepository<Project> _repository;
        readonly InMemoryDBContext _inMemoryDB;
        readonly ProjectDbContext _context;
        public ProjectServiceTests()
        {
            _mapper = MapperConfigurations();
            _inMemoryDB = new InMemoryDBContext();
            _context = _inMemoryDB.GetEmptyDbContext();
            _repository = new ProjectRepository(_context) ;
            _service = new ProjectService(_repository, _mapper);
        }
        public void Dispose()
        {
            
        }
        [Fact]
        public async Task CreateProject_ShouldWork()
        {
            using (var context = _inMemoryDB.GetEmptyDbContext())
            {
                var project = new ProjectDto { Name = Guid.NewGuid().ToString(), Description = "des" };

                var createdProject  = await _service.Create(project);

                Assert.Equal(project.Name, createdProject.Name);
            }
        }
        [Fact]
        public async Task TasksInProjectByUser_ShouldWork()
        {
            using (var context = _inMemoryDB.GetEmptyDbContext()) 
            { 
                var project = new Project { Id = 1, Name = "Project1", Description = "des", AuthorId = 1 };
                var task1 = new TaskEntity { Id = 1, Name = "Task1", Description = "task1 des", ProjectId = 1 };
                var task2 = new TaskEntity { Id = 2, Name = "Task2", Description = "task2 des", ProjectId = 1 };

                context.Projects.AddRange(project);
                context.Tasks.AddRange(task1, task2);
                context.SaveChanges();

                var res = await _service.TasksInProjectByUser(1);
                var actual = res.ToList();
                Assert.Single(actual);
                Assert.Equal(project.Name, actual[0].Key.Name);
                Assert.Equal(2, actual[0].Value);
            }
        }
        [Fact]
        public async Task AllProjectsWithTheLongestTaskAndTheShortest_ShouldWork()
        {
            using (var context = _inMemoryDB.GetEmptyDbContext()) 
            { 
                var project1 = new Project { Id = 1, Name = "Project1", Description = "des", AuthorId = 1 };
                var task1 = new TaskEntity { Id = 1, Name = "Task1", Description = "task1 short description", ProjectId = 1 };
                var task2 = new TaskEntity { Id = 2, Name = "Task2", Description = "task2 long deeeeeeeeeeescription", ProjectId = 1 };
                context.Projects.AddRange(project1);
                context.Tasks.AddRange(task1, task2);
                context.SaveChanges();
                var res = await _service.AllProjectsWithTheLongestTaskAndTheShortest();
                var actual = res.ToList();

                Assert.Single(actual);
                Assert.Equal(project1.Name, actual[0].Project.Name);
                Assert.Equal("task1 short description", actual[0].TheShortestTask.Description);
                Assert.Equal("task2 long deeeeeeeeeeescription", actual[0].TheLongestTask.Description);
            }
        }
        [Fact]
        public async Task AllProjectsWithTheLongestTaskAndTheShortest_ShouldFail()
        {
            using (var context = _inMemoryDB.GetEmptyDbContext()) 
            { 
                var project1 = new Project { Id = 1, Name = "Project1", Description = "des", AuthorId = 1 };

                _context.Projects.AddRange(project1);
                _context.SaveChanges();
                var res = await _service.AllProjectsWithTheLongestTaskAndTheShortest();
                var actual = res.ToList();

                Assert.Single(actual);
                Assert.Equal(project1.Name, actual[0].Project.Name);
                Assert.Null(actual[0].TheShortestTask);
                Assert.Null(actual[0].TheLongestTask);
            }
        }
        [Fact]
        public async Task AllNotFinishedTasksForEveryProject_ShouldWork()
        {
            int id = 1;
            using (var context = _inMemoryDB.GetDbContext()) 
            {
                var res = await _service.AllNotFinishedTasksForEveryProject(id);
                var actual = res.ToList();
                Assert.True(actual.All(item => item.Tasks.All(task => task.PerformerId == id)));
                Assert.True(actual.All(item => item.Tasks.All(task => task.State != TaskStates.Finished)));
            }
        }
        [Theory]
        [InlineData(1, 1)]
        [InlineData(1, 2)]
        [InlineData(2, 2)]
        public async Task AllNotFinishedTasksForEveryProject_AddOneMoreTask_To_Project(int userId, int projectId)
        {
            using (var context = _inMemoryDB.GetDbContext()) 
            {
                var createTask = new TaskEntity
                {
                    Name = "Task Name",
                    Description = "Task Description 1",
                    ProjectId = projectId,
                    PerformerId = userId,
                };
                var resBefore = await _service.AllNotFinishedTasksForEveryProject(userId);
                var listBefore = resBefore.ToList();
                context.Tasks.Add(createTask);
                context.SaveChanges();
                var resAfter = await _service.AllNotFinishedTasksForEveryProject(userId);
                var listAfter = resAfter.ToList();

                var before = listBefore.FirstOrDefault(x => x.Project.Id == projectId)?.Tasks.Count() ?? 0;
                var after = listAfter.FirstOrDefault(x => x.Project.Id == projectId).Tasks.Count();

                Assert.Equal(before + 1, after);
            }
        }
        [Theory]
        [InlineData(-1)]
        [InlineData(0)]
        [InlineData(9999)]
        public async Task AllNotFinishedTasksForEveryProject_ShouldBeEmpty_IncorrectUserId(int id)
        {
            using (var context = _inMemoryDB.GetDbContext()) 
            {
                var resBefore = await _service.AllNotFinishedTasksForEveryProject(id);
                var listTasksAndProject = resBefore.ToList();
                Assert.Empty(listTasksAndProject);
            }
        }
        public IMapper MapperConfigurations()
        {
            var mappingConfig = new MapperConfiguration(mc =>
            {
                mc.AddProfile(new ProjectProfile());
                mc.AddProfile(new TaskProfile());
                mc.AddProfile(new TeamProfile());
                mc.AddProfile(new UserProfile());
            });
            return mappingConfig.CreateMapper();
        }
    }
}
