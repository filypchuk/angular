﻿using System.Collections.Generic;
using System.Threading.Tasks;
using BusinessLogicLayer.Interfaces;
using Common.DTO.DtoForSevenLinqMethods;
using Common.DTO.Task;
using Microsoft.AspNetCore.Mvc;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TasksController : ControllerBase
    {
        private readonly ITaskService _taskService;

        public TasksController(ITaskService taskService)
        {
            _taskService = taskService;
        }

        [HttpGet]
        public async Task<IEnumerable<TaskDto>> GetAll()
        {
            return await _taskService.GetAll();
        }
  
        [HttpGet("{id}")]
        public async Task<ActionResult<TaskDto>> Get(int id)
        {

            var dto = await _taskService.GetById(id);
            if (dto != null)
                return Ok(dto);
            return NotFound();
        }
      
        [HttpPost]
        public async Task<ActionResult<TaskDto>> Post([FromBody] TaskDto createDto)
        {
            var dto = await _taskService.Create(createDto);
            return Created($"{dto.Id}", dto);
        }

        [HttpPut]
        public async Task<ActionResult<TaskDto>> Put([FromBody] TaskDto updateDto)
        {
            await _taskService.Update(updateDto);
            return Ok(await _taskService.GetById(updateDto.Id));

        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            var dto = await _taskService.GetById(id);
            if (dto == null)
                return NotFound();
            await _taskService.Delete(id);
            return NoContent();
        }

        [HttpGet("task-by-user/{id}")]
        public async Task<IEnumerable<TaskDto>> TasksByUser(int id)
        {
            return await _taskService.TasksByUser(id);
        }
        [HttpGet("task-finished-by-user/{id}")]
        public async Task<IEnumerable<TasksFinishedByUserDto>> TasksFinishedByUser(int id)
        {
            return await _taskService.TasksFinishedByUser(id);
        }
    }
}
