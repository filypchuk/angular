﻿using System.Collections.Generic;
using System.Threading.Tasks;
using BusinessLogicLayer.Interfaces;
using Common.DTO.DtoForSevenLinqMethods;
using Common.DTO.Team;
using Microsoft.AspNetCore.Mvc;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TeamsController : ControllerBase
    {
        private readonly ITeamService _teamService;

        public TeamsController(ITeamService teamService)
        {
            _teamService = teamService;
        }

        [HttpGet]
        public async Task<IEnumerable<TeamDto>> GetAll()
        {
            return await _teamService.GetAll();
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<TeamDto>> Get(int id)
        {

            var dto = await _teamService.GetById(id);
            if (dto != null)
                return Ok(dto);
            return NotFound();
        }

        [HttpPost]
        public async Task<ActionResult<TeamDto>> Post([FromBody] TeamDto createDto)
        {
            var dto = await _teamService.Create(createDto);
            return Created($"{dto.Id}", dto);
        }

        [HttpPut]
        public async Task<ActionResult<TeamDto>> Put([FromBody] TeamDto updateDto)
        {
            await _teamService.Update(updateDto);
            return Ok(await _teamService.GetById(updateDto.Id));

        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            var dto = await _teamService.GetById(id);
            if (dto == null)
                return NotFound();
            await _teamService.Delete(id);
            return NoContent();
        }
        [HttpGet("team-and-users")]
        public async Task<IEnumerable<TeamAndUsersDto>> TeamAndUsers()
        {
            return await _teamService.TeamAndUsersOlderTenYears();
        }
    }
}
