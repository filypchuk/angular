﻿using System.Collections.Generic;
using System.Threading.Tasks;
using BusinessLogicLayer.Interfaces;
using Common.DTO.DtoForSevenLinqMethods;
using Common.DTO.User;
using Microsoft.AspNetCore.Mvc;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly IUserService _userService;

        public UsersController(IUserService userService)
        {
            _userService = userService;
        }

        [HttpGet]
        public async Task<IEnumerable<UserDto>> GetAll()
        {
            return await _userService.GetAll();
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<UserDto>> Get(int id)
        {

            var dto = await _userService.GetById(id);
            if (dto != null)
                return Ok(dto);
            return NotFound();
        }

        [HttpPost]
        public async Task<ActionResult<UserDto>> Post([FromBody] UserDto createDto)
        {
            var dto = await _userService.Create(createDto);
            return Created($"{dto.Id}", dto);
        }

        [HttpPut]
        public async Task<ActionResult<UserDto>> Put([FromBody] UserDto updateDto)
        {
            
            await _userService.Update(updateDto);
            return Ok(await _userService.GetById(updateDto.Id));
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            var dto = await _userService.GetById(id);
            if (dto == null)
                return NotFound();
            await _userService.Delete(id);
            return NoContent();
        }
        [HttpGet("user-by-alphabet")]
        public async Task<IEnumerable<UserByAlphabetAndTasksDto>> UserByAlphabet()
        {
            return await _userService.UserByAlphabetAndTasks();
        }
        [HttpGet("user-last-project/{id}")]
        public async Task<UsersProjectTaskDto> UserLastProject(int id)
        {
            return await _userService.UsersLastProjectAndTasks(id);
        }
    }
}
