﻿namespace Common.Enums
{
    public enum TaskStates
    {
        Created = 0,
        Started,
        Finished,
        Canceled
    }
}
