﻿namespace Common.DTO.DtoForSevenLinqMethods
{
    public sealed class TasksFinishedByUserDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
