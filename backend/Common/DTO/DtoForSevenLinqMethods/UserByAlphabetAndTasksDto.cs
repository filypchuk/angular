﻿using Common.DTO.Task;
using Common.DTO.User;
using System.Collections.Generic;

namespace Common.DTO.DtoForSevenLinqMethods
{
    public sealed class UserByAlphabetAndTasksDto
    {
        public UserDto User { get; set; }
        public List<TaskDto> Tasks { get; set; }
    }
}
