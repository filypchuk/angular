﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Common.DTO.Project
{
    public sealed class ProjectDto
    {
        [Required]
        public int Id { get; set; }
        [Required]
        [MaxLength(128)]
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime Deadline { get; set; }
        public int? AuthorId { get; set; }
        public int? TeamId { get; set; }
        public override string ToString()
        {
            return $" Id -- {Id}\n Name -- {Name}\n" +
                $" CreatedAt -- {CreatedAt}\n FinishedAt -- {Deadline}\n" +
                $" AuthorId -- {AuthorId}\n TeamId -- {TeamId}\n Description -- \"{Description}\" \n";
        }
    }
}
