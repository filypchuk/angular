﻿using System;
using System.Collections.Generic;

namespace DataAccessLayer.Entities
{
    public class User
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public DateTime Birthday { get; set; }
        public DateTime RegisteredAt { get; set; }
        public int? TeamId { get; set; }
        public Team Team { get; set; }
        public ICollection<Project> Projects { get; set; }
        public ICollection<TaskEntity> Tasks { get; set; }
    }
}
