﻿using DataAccessLayer.Context;
using DataAccessLayer.Entities;
using DataAccessLayer.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DataAccessLayer.Repositories
{
    public class TeamRepository : IRepository<Team>
    {
        private protected readonly ProjectDbContext _context;

        public TeamRepository(ProjectDbContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<Team>> GetAllAsync()
        {
            return await Task.Run(() => _context.Teams
                .Include(t=>t.Users));
        }

        public async Task<Team> GetByIdAsync(int id)
        {
            return await _context.Teams.FindAsync(id);
        }

        public async Task<Team> CreateAsync(Team entity)
        {
            await _context.Teams.AddAsync(entity);
            return entity;
        }

        public async Task UpdateAsync(Team entity)
        {
            await Task.Run(() => _context.Teams.Update(entity));
        }

        public async Task DeleteAsync(int id)
        {
            Team entity = await _context.Teams.FindAsync(id);
            if (entity != null)
                await Task.Run(() => _context.Teams.Remove(entity));
        }
        public async Task SaveAsync()
        {
            await _context.SaveChangesAsync();
        }
    }
}
