﻿using DataAccessLayer.Context;
using DataAccessLayer.Entities;
using DataAccessLayer.Repositories.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DataAccessLayer.Repositories
{
    public class TaskRepository : IRepository<TaskEntity>
    {
        private protected readonly ProjectDbContext _context;

        public TaskRepository(ProjectDbContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<TaskEntity>> GetAllAsync()
        {
            return await Task.Run(() => _context.Tasks);
        }

        public async Task<TaskEntity> GetByIdAsync(int id)
        {
            return await _context.Tasks.FindAsync(id);
        }

        public async Task<TaskEntity> CreateAsync(TaskEntity entity)
        {
            await _context.Tasks.AddAsync(entity);
            return entity;
        }

        public async Task UpdateAsync(TaskEntity entity)
        {
            await Task.Run(() => _context.Tasks.Update(entity));      
        }

        public async Task DeleteAsync(int id)
        {
            TaskEntity entity = await _context.Tasks.FindAsync(id);
            if (entity != null)
                await Task.Run(() => _context.Tasks.Remove(entity));
        }
        public async Task SaveAsync()
        {
            await _context.SaveChangesAsync();
        }
    }
}
