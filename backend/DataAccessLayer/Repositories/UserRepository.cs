﻿using DataAccessLayer.Context;
using DataAccessLayer.Entities;
using DataAccessLayer.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DataAccessLayer.Repositories
{
    public class UserRepository : IRepository<User>
    {
        private protected readonly ProjectDbContext _context;

        public UserRepository(ProjectDbContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<User>> GetAllAsync()
        {
            return await Task.Run(() => _context.Set<User>()
                .Include(u => u.Projects)
                    .ThenInclude(p => p.Tasks)
                .Include(u => u.Tasks));
        }

        public async Task<User> GetByIdAsync(int id)
        {
            return await _context.Users.FindAsync(id);
        }

        public async Task<User> CreateAsync(User entity)
        {
            await _context.Users.AddAsync(entity);
            return entity;
        }

        public async Task UpdateAsync(User entity)
        {
            await Task.Run(() => _context.Users.Update(entity));
        }

        public async Task DeleteAsync(int id)
        {
            User entity = _context.Users.Find(id);
            if (entity != null)
                await Task.Run(() => _context.Users.Remove(entity));
        }
        public async Task SaveAsync()
        {
            await _context.SaveChangesAsync();
        }
    }
}
