﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleClient.HttpRequests
{
    public class HttpRequest
    {
        private const string URL = "https://localhost:5001/api";
        private readonly HttpClient _client;
        public HttpRequest()
        {
            _client = new HttpClient();
        }
        public async Task<List<T>> GetAll<T>(string partUrl)
        {
            List<T> list = new List<T>();
            try
            {
                var response = await _client.GetAsync($"{URL}/{partUrl}");
                Console.WriteLine($"--------|GET: {URL}/{partUrl} |--------");
                Console.WriteLine(response);
                Console.WriteLine("---------------------------------------------------------------------");
                string json = await response.Content.ReadAsStringAsync();
                list = JsonConvert.DeserializeObject<List<T>>(json);
                return list;
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex);
                return list;
            }

        }
        public async Task<T> GetById<T>(string partUrl, int id)
        {
            try
            {
                var response = await _client.GetAsync($"{URL}/{partUrl}/{id}");
                Console.WriteLine($"--------|GET: {URL}/{partUrl}/{id} |--------");
                Console.WriteLine(response);
                Console.WriteLine("---------------------------------------------------------------------");

                string json = await response.Content.ReadAsStringAsync();
                var dto = JsonConvert.DeserializeObject<T>(json);
                return dto;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return default;
            }
        }
        public async Task<T> Create<T>(string partUrl, string jsonObj)
        {
            try
            {
                HttpContent stringContent = new StringContent(jsonObj, Encoding.UTF8, "application/json");
                var response = await _client.PostAsync($"{URL}/{partUrl}", stringContent);
                Console.WriteLine($"--------|POST: {URL}/{partUrl} |--------");
                Console.WriteLine($"--------|JSON: {jsonObj}|--------");
                Console.WriteLine(response);
                Console.WriteLine("---------------------------------------------------------------------");
                string json = await response.Content.ReadAsStringAsync();
                var dto = JsonConvert.DeserializeObject<T>(json);
                return dto;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return default;
            }
        }
        public async Task<T> Update<T>(string partUrl, string jsonObj)
        {
            try
            {
                HttpContent stringContent = new StringContent(jsonObj, Encoding.UTF8, "application/json");
                var response = await _client.PutAsync($"{URL}/{partUrl}", stringContent);
                Console.WriteLine($"--------|PUT: {URL}/{partUrl} |--------");
                Console.WriteLine($"--------|JSON: {jsonObj}|--------");
                Console.WriteLine(response);
                Console.WriteLine("---------------------------------------------------------------------");
                string json = await response.Content.ReadAsStringAsync();
                var dto = JsonConvert.DeserializeObject<T>(json);
                return dto;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return default;
            }
        }
        public async Task Delete(string partUrl, int id)
        {
            try
            {
                var response = await _client.DeleteAsync($"{URL}/{partUrl}/{id}");
                Console.WriteLine($"--------|DELETE: {URL}/{partUrl}/{id} |--------");
                Console.WriteLine(response.StatusCode);
                Console.WriteLine("---------------------------------------------------------------------");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }
    }
}
