﻿using ConsoleClient.ClientService;
using ConsoleClient.Helpers;
using System;

namespace ConsoleClient.View
{
    public class QueriesView
    {
        private readonly PrintToConsole printToConsole;
        private readonly Printing<string, Color> print;
        private readonly QueriesService queries;
        public QueriesView()
        {
            printToConsole = new PrintToConsole();
            print = printToConsole.PrintColor;
            queries = new QueriesService();       
        }
        public async void MarkedTaskId()
        {
            try 
            { 
                var markedTaskId = await queries.MarkRandomTaskWithDelay(1000);
                print(markedTaskId.ToString(), Color.Blue);
            }
            catch (Exception e)
            {
                print("\n-------------------------------------------------\n", Color.Red);
                print(e.ToString(), Color.Red);
            }
        }
    }
}
