﻿using Common.Enums;
using System;
using System.Linq;
using System.Threading.Tasks;
using System.Timers;

namespace ConsoleClient.ClientService
{
    public class QueriesService
    {
        private readonly Random rand;
        private readonly TaskClientService _taskClientService;
        public QueriesService()
        {
            rand = new Random();
            _taskClientService = new TaskClientService();
        }
        public async Task<int> MarkRandomTaskWithDelay(double interval) 
        {
            Timer aTimer = new Timer(interval) { AutoReset = false,  Enabled = true };
            var tcs = new TaskCompletionSource<int> ();

            aTimer.Elapsed += async (o, args) => 
            {
                try
                {
                    tcs.SetResult(await DoOperations());
                }
                catch (Exception e)
                {
                    tcs.SetException(e);
                }
            };

            return await tcs.Task;
        }
        private async Task<int> DoOperations()
        {
            var list = await _taskClientService.GetAll();
            if (list.Count == 0)
                throw new ArgumentNullException("List task is empty");

            var filteredList = list.Where(x => x.State != TaskStates.Finished).ToList();
            if(filteredList.Count == 0)
                throw new Exception("All task finished");

            int randomIndex = rand.Next(0, filteredList.Count);
            var dto = filteredList[randomIndex];
            dto.State = TaskStates.Finished;
            var upadtedDto = await _taskClientService.Update(dto);
            return upadtedDto.Id;
        }
    }
}
