﻿using Common.DTO.DtoForSevenLinqMethods;
using Common.DTO.User;
using ConsoleClient.HttpRequests;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ConsoleClient.ClientService
{
    public class UserClientService
    {
        private readonly HttpRequest _request;
        public UserClientService()
        {
            _request = new HttpRequest();
        }
        public async Task<List<UserDto>> GetAll()
        {
            return await _request.GetAll<UserDto>("users");
        }
        public async Task<UserDto> GetById(int id)
        {
            return await _request.GetById<UserDto>("users", id);
        }
        public async Task<UserDto> Create(UserCreateDto createDto)
        {
            var json = JsonConvert.SerializeObject(createDto);
            return await _request.Create<UserDto>("users", json);
        }
        public async Task<UserDto> Update(UserDto updateDto)
        {
            var json = JsonConvert.SerializeObject(updateDto);
            return await _request.Update<UserDto>("users", json);
        }
        public async Task Delete(int id)
        {
            await _request.Delete("users", id);
        }
        public async Task<List<UserByAlphabetAndTasksDto>> UserByAlphabet()
        {
            return await _request.GetAll<UserByAlphabetAndTasksDto>("users/user-by-alphabet");
        }
        public async Task<UsersProjectTaskDto> UserLastProject(int userId)
        {
            return await _request.GetById<UsersProjectTaskDto>("users/user-last-project", userId);
        } 
    }
}
