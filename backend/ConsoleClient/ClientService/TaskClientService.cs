﻿using Common.DTO.DtoForSevenLinqMethods;
using Common.DTO.Task;
using ConsoleClient.HttpRequests;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ConsoleClient.ClientService
{
    public class TaskClientService
    {
        private readonly HttpRequest _request;
        public TaskClientService()
        {
            _request = new HttpRequest();
        }
        public async Task<List<TaskDto>> GetAll()
        {
            return await _request.GetAll<TaskDto>("tasks");
        }
        public async Task<TaskDto> GetById(int id)
        {
            return await _request.GetById<TaskDto>("tasks", id);
        }
        public async Task<TaskDto> Create(TaskCreateDto createDto)
        {
            var json = JsonConvert.SerializeObject(createDto);
            return await _request.Create<TaskDto>("tasks", json);
        }
        public async Task<TaskDto> Update(TaskDto updateDto)
        {
            var json = JsonConvert.SerializeObject(updateDto);
            return await _request.Update<TaskDto>("tasks", json);
        }
        public async Task Delete(int id)
        {
            await _request.Delete("tasks", id);
        }
        public async Task<List<TaskDto>> TasksByUser(int id)
        {
            return await _request.GetById<List<TaskDto>>("tasks/task-by-user", id); 
        }
        public async Task<List<TasksFinishedByUserDto>> TasksFinishedByUser(int id)
        {
            return await _request.GetById<List<TasksFinishedByUserDto>>("tasks/task-finished-by-user", id);
        }
    }
}
