﻿using System;

namespace ConsoleClient.Helpers
{
    public delegate void Printing<S, C>(string str, C color);
    public delegate void WaitingForEnter();
    class PrintToConsole
    {
        public void PrintColor(string str, Color color)
        {
            switch (color)
            {
                case Color.Red:
                    Console.ForegroundColor = ConsoleColor.Red;
                    break;
                case Color.Green:
                    Console.ForegroundColor = ConsoleColor.DarkGreen;
                    break;
                case Color.Yellow:
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    break;
                case Color.Blue:
                    Console.ForegroundColor = ConsoleColor.Blue;
                    break;
                case Color.None:
                    Console.ResetColor();
                    break;
                default:
                    Console.ForegroundColor = ConsoleColor.Gray;
                    break;
            }
            Console.WriteLine(str);
            Console.ResetColor();
        }
        public void WaitEnter()
        {
            PrintColor("Click ENTER button for continue", Color.Blue);
            Console.ReadLine();
        }
    }
}
