﻿using ConsoleClient.Helpers;
using ConsoleClient.View;
using System;
using System.Threading.Tasks;

namespace ConsoleClient.ConsoleMenu
{
    public class TeamMenu
    {
        private readonly PrintToConsole printToConsole;
        private readonly Printing<string, Color> print;
        private readonly TeamView view;
        public TeamMenu()
        {
            printToConsole = new PrintToConsole();
            print = printToConsole.PrintColor;
            view = new TeamView();
        }
        private void DisplayTeamMenu()
        {
            Console.Clear();
            print("Click button 1-6 to select \n", Color.Red);
            print("1---Get all Team \n", Color.Green);
            print("2---Get Team by id \n", Color.Green);
            print("3---Create Team \n", Color.Blue);
            print("4---Update Team \n", Color.Yellow);
            print("5---Delete Team \n", Color.Red);
            print("6---Get a list (id, team name and user list) from a collection of teams over 10 years old \n", Color.Green);
            print("\n   Click Esc to back menu", Color.Red);
        }
        public async Task StartTeamMenu()
        {
            while (true)
            {
                DisplayTeamMenu();
                var keyInfo = Console.ReadKey(true);
                switch (keyInfo.Key)
                {
                    case ConsoleKey.D1:
                        await view.AllTeams();
                        break;
                    case ConsoleKey.D2:
                        await view.TeamById();
                        break;
                    case ConsoleKey.D3:
                        await view.CreateTeam();
                        break;
                    case ConsoleKey.D4:
                        await view.UpdateTeam();
                        break;
                    case ConsoleKey.D5:
                        await view.Delete();
                        break;
                    case ConsoleKey.D6:
                        await view.TeamAndUsers();
                        break;
                    case ConsoleKey.Escape:
                        return;
                    default:
                        break;
                }
            }
        }
    }
}
