﻿using ConsoleClient.Helpers;
using ConsoleClient.View;
using System;
using System.Threading.Tasks;

namespace ConsoleClient.ConsoleMenu
{
    class MainMenu
    {
        private readonly PrintToConsole printToConsole;
        private readonly Printing<string, Color> print;
        private readonly ProjectMenu projectMenu;
        private readonly TaskMenu taskMenu;
        private readonly TeamMenu teamMenu;
        private readonly UserMenu userMenu;
        private readonly QueriesView queries;
        public MainMenu()
        {
            printToConsole = new PrintToConsole();
            print = printToConsole.PrintColor;
            projectMenu = new ProjectMenu();
            taskMenu = new TaskMenu();
            teamMenu = new TeamMenu();
            userMenu = new UserMenu();
            queries = new QueriesView();
        }
        public async Task Start()
        {            
            while (true)
            {
                DisplayMenu();
                var keyInfo = Console.ReadKey(true);
                switch (keyInfo.Key)
                {
                    case ConsoleKey.D1:
                        await projectMenu.StartProjectMenu();
                        break;
                    case ConsoleKey.D2:
                        await taskMenu.StartTaskMenu();
                        break;
                    case ConsoleKey.D3:
                        await teamMenu.StartTeamMenu();
                        break;
                    case ConsoleKey.D4:
                        await userMenu.StartUserMenu();
                        break;
                    case ConsoleKey.D5:
                        queries.MarkedTaskId();
                        break;
                    default:
                        break;
                }              
            }
        }
        private void DisplayMenu()
        {
            Console.Clear();
            print("Click button 1-5 to select \n", Color.Red);
            print("1---Projects ", Color.Green);
            print("2---Tasks ", Color.Green);
            print("3---Teams ", Color.Green);
            print("4---Users ", Color.Green);
            print("5---Mark Random Task ", Color.Blue);
        }       
    }
}
