﻿using DataAccessLayer.Entities;
using System.Collections.Generic;

namespace BusinessLogicLayer.ModelsForSevenMethods
{
    public sealed class TeamAndUsers
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public List<User> Users { get; set; }
    }

}
