﻿using DataAccessLayer.Entities;

namespace BusinessLogicLayer.ModelsForSevenMethods
{
    public sealed class ProjectAndCountTasks
    {
        public Project Key { get; set; }
        public int Value { get; set; }
    }
}
