﻿using AutoMapper;
using BusinessLogicLayer.Interfaces;
using BusinessLogicLayer.Services.Abstract;
using Common.DTO.DtoForSevenLinqMethods;
using Common.DTO.Task;
using DataAccessLayer.Entities;
using DataAccessLayer.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BusinessLogicLayer.Services
{
    public class TaskService : BaseService, ITaskService
    {
        private readonly IRepository<TaskEntity> _repository;
        public TaskService(IRepository<TaskEntity> repository, IMapper mapper) : base( mapper) 
        {
            _repository = repository;
        }
        public async Task<IEnumerable<TaskDto>> GetAll()
        {
            return _mapper.Map<IEnumerable<TaskDto>>(await _repository.GetAllAsync());
        }

        public async Task<TaskDto> GetById(int id)
        {
            return _mapper.Map<TaskDto>(await _repository.GetByIdAsync(id));
        }
        public async Task<TaskDto> Create(TaskDto dto)
        {
            var newTask = _mapper.Map<TaskEntity>(dto);
            newTask.CreatedAt = DateTime.Now;
            var created = await _repository.CreateAsync(newTask);
            await _repository.SaveAsync();
            return _mapper.Map<TaskDto>(created);
        }

        public async Task Update(TaskDto dto)
        {
            var entityUpdate = _mapper.Map<TaskEntity>(dto);
            await _repository.UpdateAsync(entityUpdate);
            await _repository.SaveAsync();
        }

        public async Task Delete(int id)
        {
            await _repository.DeleteAsync(id);
            await _repository.SaveAsync();
        }
        public async Task<IEnumerable<TaskDto>> TasksByUser(int userId)
        {
            var enumerable = await _repository.GetAllAsync();
             var tasks = enumerable.Where(task => task.PerformerId == userId && task.Name.Length < 45);

            return _mapper.Map<IEnumerable<TaskDto>>(tasks);
        }
        public async Task<List<TasksFinishedByUserDto>> TasksFinishedByUser(int userId)
        {
            var enumerable = await _repository.GetAllAsync();

            return enumerable
                    .Where(task => task.PerformerId == userId && task.FinishedAt.Year == DateTime.Now.Year)
                    .Select(t => new TasksFinishedByUserDto { Id = t.Id, Name = t.Name }).ToList();
        }     
    }
}
