﻿using AutoMapper;
using BusinessLogicLayer.Interfaces;
using BusinessLogicLayer.Services.Abstract;
using Common.DTO.DtoForSevenLinqMethods;
using Common.DTO.Team;
using DataAccessLayer.Entities;
using BusinessLogicLayer.ModelsForSevenMethods;
using DataAccessLayer.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BusinessLogicLayer.Services
{
    public class TeamService : BaseService, ITeamService
    {
        private readonly IRepository<Team> _teamRepository;
        public TeamService(IRepository<Team> teamRepository, IMapper mapper) : base(mapper) 
        {
            _teamRepository = teamRepository;
        }
        public async Task<List<TeamDto>> GetAll()
        {
            return _mapper.Map<List<TeamDto>>(await _teamRepository.GetAllAsync());
        }

        public async Task<TeamDto> GetById(int id)
        {
            return _mapper.Map<TeamDto>(await _teamRepository.GetByIdAsync(id));
        }
        public async Task<TeamDto> Create(TeamDto dto)
        {
            var newTeam = _mapper.Map<Team>(dto);
            newTeam.CreatedAt = DateTime.Now;
            var created = await _teamRepository.CreateAsync(newTeam);
           await _teamRepository.SaveAsync();
            return _mapper.Map<TeamDto>(created);
        }
        public async Task Update(TeamDto dto)
        {
            var entityUpdate = _mapper.Map<Team>(dto);
            await _teamRepository.UpdateAsync(entityUpdate);
            await _teamRepository.SaveAsync();
        }
        public async Task Delete(int id)
        {
            await _teamRepository.DeleteAsync(id);
            await _teamRepository.SaveAsync();
        }
        public async Task<List<TeamAndUsersDto>> TeamAndUsersOlderTenYears()
        {
            var enumerable = await _teamRepository.GetAllAsync();
            var teamAndUsers = enumerable
                .Select(team =>
                        new TeamAndUsers
                        {
                            Id = team.Id,
                            Name = team.Name,
                            Users = team.Users.Where(u => u.Birthday.Year <= DateTime.Now.Year - 10)
                                    .OrderByDescending(u => u.RegisteredAt).ToList()
                        })
                .Where(team => team.Users.Any(u => u.Birthday.Year <= DateTime.Now.Year - 10));

            return _mapper.Map<List<TeamAndUsersDto>>(teamAndUsers);
        }
    }
}
