﻿using AutoMapper;
using BusinessLogicLayer.Interfaces;
using BusinessLogicLayer.Services.Abstract;
using Common.DTO.DtoForSevenLinqMethods;
using Common.DTO.Project;
using DataAccessLayer.Entities;
using BusinessLogicLayer.ModelsForSevenMethods;
using DataAccessLayer.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using Common.Enums;
using System.Threading.Tasks;

namespace BusinessLogicLayer.Services
{
    public class ProjectService : BaseService, IProjectService
    {
        private readonly IRepository<Project> _projectRepository;
        public ProjectService(IRepository<Project> projRepository, IMapper mapper) : base(mapper)
        {
            _projectRepository = projRepository;
        }
        public async Task<List<ProjectDto>> GetAll()
        {
            return _mapper.Map<List<ProjectDto>>(await _projectRepository.GetAllAsync());
        }
        public async Task<ProjectDto> GetById(int id)
        {
            return _mapper.Map<ProjectDto>(await _projectRepository.GetByIdAsync(id));
        }
        public async Task<ProjectDto> Create(ProjectDto dto)
        {
            var newTeam = _mapper.Map<Project>(dto);
            newTeam.CreatedAt = DateTime.Now;
            var created = await _projectRepository.CreateAsync(newTeam);
            await _projectRepository.SaveAsync();
            return _mapper.Map<ProjectDto>(created);
        }
        public async Task Update(ProjectDto dto)
        {
            var entityUpdate = _mapper.Map<Project>(dto);
            await _projectRepository.UpdateAsync(entityUpdate);
            await _projectRepository.SaveAsync();

        }
        public async Task Delete(int id)
        {
            await _projectRepository.DeleteAsync(id);
            await _projectRepository.SaveAsync();
        }
        public async Task<List<ProjectAndCountTasksDto>> TasksInProjectByUser(int userId)
        {
           var enumerable = await _projectRepository.GetAllAsync();
           var projectsAndCount = enumerable.Where(p => p.AuthorId == userId)
                     .Select(x =>
                     new ProjectAndCountTasks
                     {
                         Key = x,
                         Value = x.Tasks.Count
                     });
            return _mapper.Map<List<ProjectAndCountTasksDto>>(projectsAndCount);
        }
        public async Task<List<ProjectAndTwoTasksDto>> AllProjectsWithTheLongestTaskAndTheShortest()
        {
           var enumerable = await _projectRepository.GetAllAsync();
           var projectsAndTasks = await Task.WhenAll(enumerable.Select(async project =>  
                new ProjectAndTwoTasks()
                {
                    Project = project,
                    TheLongestTask = await Task.Run(() => project.Tasks.OrderBy(task => task.Description.Length).LastOrDefault()),
                    TheShortestTask = await Task.Run(() => project.Tasks.OrderBy(task => task.Name.Length).FirstOrDefault()),
                    CountUsersInTeam = await Task.Run(() => project.Team?.Users.Where(u => project.Description.Length > 20 || (project.Tasks.Count) < 3).Count() ?? 0)
                }));
            return _mapper.Map<List<ProjectAndTwoTasksDto>>(projectsAndTasks);
        }
        public async Task<List<NotFinishedTasksByUserIdDto>> AllNotFinishedTasksForEveryProject(int userId)
        {
            var enumerable = await _projectRepository.GetAllAsync();
            var projectAndTasks = enumerable.Select(project => new NotFinishedTasksByUserId
                {
                    Project = project,
                    Tasks = project.Tasks.Where(t => (t.State != TaskStates.Finished) && t.PerformerId == userId).ToList(),
                }).Where(p=>p.Tasks.Count() != 0);

            return _mapper.Map<List<NotFinishedTasksByUserIdDto>>(projectAndTasks);
        }
    }
}
