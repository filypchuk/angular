﻿using AutoMapper;
using BusinessLogicLayer.Interfaces;
using BusinessLogicLayer.Services.Abstract;
using Common.DTO.DtoForSevenLinqMethods;
using Common.DTO.User;
using BusinessLogicLayer.ModelsForSevenMethods;
using DataAccessLayer.Entities;
using Common.Enums;
using DataAccessLayer.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BusinessLogicLayer.Services
{
    public class UserService : BaseService, IUserService
    {
        private readonly IRepository<User> _userRepository;

        public UserService(IRepository<User> userRepository, IMapper mapper) : base( mapper) 
        {
            _userRepository = userRepository;
        }
        public async Task<List<UserDto>> GetAll()
        {
            return _mapper.Map<List<UserDto>>(await _userRepository.GetAllAsync());
        }

        public async Task<UserDto> GetById(int id)
        {
            return _mapper.Map<UserDto>(await _userRepository.GetByIdAsync(id));
        }
        public async Task<UserDto> Create(UserDto dto)
        {
            var newUser = _mapper.Map<User>(dto);
            newUser.RegisteredAt = DateTime.Now;
            await _userRepository.CreateAsync(newUser);
            await _userRepository.SaveAsync();
            return _mapper.Map<UserDto>(newUser); 
        }

        public async Task Update(UserDto dto)
        {
            var entityUpdate = _mapper.Map<User>(dto);
            await _userRepository.UpdateAsync(entityUpdate);
            await _userRepository.SaveAsync();
        }
        public async Task Delete(int id)
        {
            await _userRepository.DeleteAsync(id);
            await _userRepository.SaveAsync();
        }
        public async Task<List<UserByAlphabetAndTasksDto>> UserByAlphabetAndTasks()
        {
            var enumerable = await _userRepository.GetAllAsync();
            var userByAlphabet = enumerable
                .Select(user => 
                  new UserByAlphabetAndTasks
                  {
                      User = user,
                      Tasks = user.Tasks.OrderByDescending(t => t.Name.Length).ToList()
                  })
                  .OrderBy(u => u.User.FirstName);
            return _mapper.Map<List<UserByAlphabetAndTasksDto>>(userByAlphabet);
        }
        public async Task<UsersProjectTaskDto> UsersLastProjectAndTasks(int userId)
        {
            var enumerable = await _userRepository.GetAllAsync();
            var usersProjectTask = await Task.WhenAll(enumerable.Where(x => x.Id == userId)
                .Select(async user =>
                    new UsersProjectTask
                    {
                        User = user,
                        LastProject = await Task.Run(() => user.Projects.OrderBy(p => p.CreatedAt).LastOrDefault()),
                        CountTasksByLastProject = await Task.Run(() => user.Projects.OrderBy(p => p.CreatedAt).LastOrDefault()?.Tasks.Count ?? 0),
                        CountStartCancelTasks = await Task.Run(() => user.Tasks.Where(t => t.State == TaskStates.Canceled || t.State == TaskStates.Started).Count()),
                        TheLongestByTimeTask = await Task.Run(() =>  user.Tasks.OrderBy(t => t.FinishedAt - t.CreatedAt).ToList().LastOrDefault())
                    }));
            return  _mapper.Map<UsersProjectTaskDto>(usersProjectTask.LastOrDefault());
        }
    }
}
