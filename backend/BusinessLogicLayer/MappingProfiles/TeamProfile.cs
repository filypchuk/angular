﻿using AutoMapper;
using Common.DTO.DtoForSevenLinqMethods;
using Common.DTO.Team;
using DataAccessLayer.Entities;
using BusinessLogicLayer.ModelsForSevenMethods;

namespace BusinessLogicLayer.MappingProfiles
{
    public class TeamProfile : Profile
    {
        public TeamProfile()
        {
            CreateMap<Team, TeamDto>();
            CreateMap<TeamDto, Team>();
            CreateMap<TeamCreateDto, Team>();
            CreateMap<TeamAndUsers, TeamAndUsersDto>();
        }
    }
}
