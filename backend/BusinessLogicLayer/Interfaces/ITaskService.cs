﻿using Common.DTO.DtoForSevenLinqMethods;
using Common.DTO.Task;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BusinessLogicLayer.Interfaces
{
    public interface ITaskService
    {
        public Task<IEnumerable<TaskDto>> GetAll();
        public Task<TaskDto> GetById(int id);
        public Task<TaskDto> Create(TaskDto dto);
        public Task Update(TaskDto dto);
        public Task Delete(int id);
        public Task<IEnumerable<TaskDto>> TasksByUser(int userId);
        public Task<List<TasksFinishedByUserDto>> TasksFinishedByUser(int userId);
    }
}
