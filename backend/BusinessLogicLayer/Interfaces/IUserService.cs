﻿using Common.DTO.DtoForSevenLinqMethods;
using Common.DTO.User;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BusinessLogicLayer.Interfaces
{
    public interface IUserService
    {
        public Task<List<UserDto>> GetAll();
        public Task<UserDto> GetById(int id);
        public Task<UserDto> Create(UserDto dto);
        public Task Update(UserDto dto);
        public Task Delete(int id);
        public Task<List<UserByAlphabetAndTasksDto>> UserByAlphabetAndTasks();
        public Task<UsersProjectTaskDto> UsersLastProjectAndTasks(int userId);
    }
}
