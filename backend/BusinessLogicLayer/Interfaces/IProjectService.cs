﻿using Common.DTO.DtoForSevenLinqMethods;
using Common.DTO.Project;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BusinessLogicLayer.Interfaces
{
    public interface IProjectService
    {
        public Task<List<ProjectDto>> GetAll();
        public Task<ProjectDto> GetById(int id);
        public Task<ProjectDto> Create(ProjectDto dto);
        public Task Update(ProjectDto dto);
        public Task Delete(int id);
        public Task<List<ProjectAndCountTasksDto>> TasksInProjectByUser(int userId);
        public Task<List<ProjectAndTwoTasksDto>> AllProjectsWithTheLongestTaskAndTheShortest();
        public Task<List<NotFinishedTasksByUserIdDto>> AllNotFinishedTasksForEveryProject(int userId);
    }
}
