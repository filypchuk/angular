﻿using Common.DTO.DtoForSevenLinqMethods;
using Common.DTO.Team;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BusinessLogicLayer.Interfaces
{
    public interface ITeamService
    {
        public Task<List<TeamDto>> GetAll();
        public Task<TeamDto> GetById(int id);
        public Task<TeamDto> Create(TeamDto dto);
        public Task Update(TeamDto dto);
        public Task Delete(int id);
        public Task<List<TeamAndUsersDto>> TeamAndUsersOlderTenYears();
    }
}