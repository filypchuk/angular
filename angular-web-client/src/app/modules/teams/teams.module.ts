import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TeamsListComponent } from './components/teams-list/teams-list.component';
import { TeamCreateComponent } from './components/team-create/team-create.component';
import { TeamUpdateComponent } from './components/team-update/team-update.component';
import { TeamItemComponent } from './components/team-item/team-item.component';
import { AppRoutingModule } from 'src/app/app-routing.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';



@NgModule({
  declarations: [TeamsListComponent, TeamCreateComponent, TeamUpdateComponent, TeamItemComponent],
  imports: [CommonModule, AppRoutingModule, SharedModule, BrowserModule, FormsModule],
  exports: [TeamsListComponent, TeamCreateComponent, TeamUpdateComponent, TeamItemComponent]
})
export class TeamsModule { }
