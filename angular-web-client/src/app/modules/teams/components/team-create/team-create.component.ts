import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Team } from 'src/app/models/team';
import { TeamService } from 'src/app/services/team.service';
import { Router } from '@angular/router';
import { ComponentCanDeactivate } from 'src/app/shared/guards/exit.guard';

@Component({
  selector: 'app-team-create',
  templateUrl: './team-create.component.html',
  styleUrls: ['./team-create.component.css']
})
export class TeamCreateComponent implements OnInit, ComponentCanDeactivate {

  saved = false;
  team: Team = { } as Team;

  constructor(private router: Router, private teamService: TeamService) { }

  ngOnInit(): void {
  }

  cancel() {
    this.saved = true;
    this.router.navigate(['/teams']);
  }
  save() {
    this.teamService.createTeam(this.team).subscribe();
    this.saved = true;
    this.router.navigate(['/teams']);
  }
  canDeactivate(): boolean | Observable<boolean>{

    if (!this.saved){
        return confirm('Do you want to leave the page?');
    }
    else{
        return true;
    }
}

}
