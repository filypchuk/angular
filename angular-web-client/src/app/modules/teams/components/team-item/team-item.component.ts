import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Team } from 'src/app/models/team';

@Component({
  selector: 'app-team-item',
  templateUrl: './team-item.component.html',
  styleUrls: ['./team-item.component.css']
})
export class TeamItemComponent implements OnInit {
  @Input() team: Team;
  @Output() deletedTeam = new EventEmitter<Team>();
  constructor() { }

  ngOnInit(): void {
  }
  delete() {
    this.deletedTeam.emit(this.team);
  }
}
