import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { TeamService } from 'src/app/services/team.service';
import { switchMap } from 'rxjs/operators';
import { Team } from 'src/app/models/team';
import { ComponentCanDeactivate } from 'src/app/shared/guards/exit.guard';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-team-update',
  templateUrl: './team-update.component.html',
  styleUrls: ['./team-update.component.css']
})
export class TeamUpdateComponent implements OnInit, ComponentCanDeactivate {
  id: number;
  saved = false;
  team: Team = { } as Team;

  constructor(private router: Router, private route: ActivatedRoute,
              private teamService: TeamService) { }

  ngOnInit(): void {
    this.route.paramMap.pipe(
      switchMap(params => params.getAll('id')))
      .subscribe(data => this.id = +data);
    this.getTeamById();
  }
  getTeamById()  {
    this.teamService.getTeamById(this.id)
    .subscribe((data: Team) => this.team = data);
   }

  cancel(){
    this.saved = true;
    this.router.navigate(['/teams']);
  }
  save(){

    this.teamService.updateTeam(this.team).subscribe();
    this.saved = true;
    this.router.navigate(['/teams']);
  }
  canDeactivate(): boolean | Observable<boolean>{

    if (!this.saved){
        return confirm('Do you want to leave the page?');
    }
    else{
        return true;
    }
}

}
