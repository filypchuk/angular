import { Component, OnInit } from '@angular/core';
import { TeamService } from 'src/app/services/team.service';
import { Team } from 'src/app/models/team';

@Component({
  selector: 'app-teams-list',
  templateUrl: './teams-list.component.html',
  styleUrls: ['./teams-list.component.css']
})
export class TeamsListComponent implements OnInit {
  teams: Team[] = [];
  constructor(private teamService: TeamService) { }

  ngOnInit(): void {
    this.getTeams();
  }
  getTeams()  {
    this.teamService.getTeams()
    .subscribe((data: Team[]) => this.teams = data);
   }
   deleteTeam(teamDelete: Team) {
     const res = confirm('Are you sure, you want to delete team?');
     if (res) {
       this.teamService.deleteTeam(teamDelete.id).subscribe();
       this.teams = this.teams.filter(item => item.id !== teamDelete.id);
     }
   }
}
