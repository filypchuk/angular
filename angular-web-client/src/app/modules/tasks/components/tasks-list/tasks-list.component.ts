import { Component, OnInit } from '@angular/core';
import { Task } from 'src/app/models/task';
import { TaskService } from 'src/app/services/task.service';
import { ProjectService } from 'src/app/services/project.service';
import { UserService } from 'src/app/services/user.service';
import { forkJoin } from 'rxjs';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-tasks-list',
  templateUrl: './tasks-list.component.html',
  styleUrls: ['./tasks-list.component.css']
})
export class TasksListComponent implements OnInit {
  tasks: Task [] = [];
  constructor(private taskService: TaskService, private projectService: ProjectService, private userService: UserService) { }

  ngOnInit(): void {
    this.getTask();
  }
  getTask()  {
    forkJoin(this.taskService.getTasks(), this.projectService.getProjects(), this.userService.getUsers())
    .pipe(map(([tasks, projects, users]) =>
    {
      tasks = tasks.map(task =>
      {
        task.user = users.find(u => u.id === task.performerId );
        task.project = projects.find(p => p.id === task.projectId );
        return task;
    });
      return tasks;
    })).subscribe(data => this.tasks = data);
   }
   deleteTask(taskDelete: Task) {
     const res = confirm('Are you sure, you want to delete task?');
     if (res) {
       this.taskService.deleteTask(taskDelete.id).subscribe();
       this.tasks = this.tasks.filter(item => item.id !== taskDelete.id);
     }
   }
}
