import { Component, OnInit } from '@angular/core';
import { ComponentCanDeactivate } from 'src/app/shared/guards/exit.guard';
import { Task } from 'src/app/models/task';
import { Project } from 'src/app/models/project';
import { User } from 'src/app/models/user';
import { ProjectService } from 'src/app/services/project.service';
import { TaskService } from 'src/app/services/task.service';
import { Router, ActivatedRoute } from '@angular/router';
import { UserService } from 'src/app/services/user.service';
import { switchMap } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-task-update',
  templateUrl: './task-update.component.html',
  styleUrls: ['./task-update.component.css']
})
export class TaskUpdateComponent implements OnInit, ComponentCanDeactivate {
  id: number;
  saved = false;
  task: Task = { } as Task;
  projects: Project[] = [];
  users: User[] = [];
  constructor(private projectService: ProjectService, private router: Router, private route: ActivatedRoute,
              private taskService: TaskService, private userService: UserService) { }

  ngOnInit(): void {
    this.route.paramMap.pipe(
      switchMap(params => params.getAll('id')))
      .subscribe(data => this.id = +data);
    this.getTaskById();
    this.getProjects();
    this.getUsers();
  }
  getTaskById()  {
    this.taskService.getTaskById(this.id)
     .subscribe((data: Task) => this.task = data);
   }
  getProjects()  {
    this.projectService.getProjects()
    .subscribe((data: Project[]) => this.projects = data);
   }

   getUsers()  {
    this.userService.getUsers()
    .subscribe((data: User[]) => this.users = data);
   }

  cancel(){
    this.saved = true;
    this.router.navigate(['/tasks']);
  }
  async save(){
    this.task.projectId = +this.task.projectId; // parse value to number
    this.task.performerId = +this.task.performerId;
    this.taskService.updateTask(this.task).subscribe();
    this.saved = true;
    this.router.navigate(['/tasks']);
  }
  canDeactivate(): boolean | Observable<boolean>{

      if (!this.saved){
          return confirm('Do you want to leave the page?');
      }
      else{
          return true;
      }
  }

}
