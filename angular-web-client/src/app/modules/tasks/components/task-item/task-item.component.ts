import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Task } from 'src/app/models/task';

@Component({
  selector: 'app-task-item',
  templateUrl: './task-item.component.html',
  styleUrls: ['./task-item.component.css']
})
export class TaskItemComponent implements OnInit {
  @Input() task: Task;
  @Output() deletedTask = new EventEmitter<Task>();
  constructor() { }

  ngOnInit(): void {
  }

  delete() {
      this.deletedTask.emit(this.task);
  }
}
