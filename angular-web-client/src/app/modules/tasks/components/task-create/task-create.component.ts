import { Component, OnInit } from '@angular/core';
import { Task } from 'src/app/models/task';
import { Project } from 'src/app/models/project';
import { User } from 'src/app/models/user';
import { ProjectService } from 'src/app/services/project.service';
import { TaskService } from 'src/app/services/task.service';
import { Router } from '@angular/router';
import { UserService } from 'src/app/services/user.service';
import { Observable } from 'rxjs';
import { ComponentCanDeactivate } from 'src/app/shared/guards/exit.guard';

@Component({
  selector: 'app-task-create',
  templateUrl: './task-create.component.html',
  styleUrls: ['./task-create.component.css']
})
export class TaskCreateComponent implements OnInit, ComponentCanDeactivate {
  saved = false;
  task: Task = { } as Task;
  projects: Project[] = [];
  users: User[] = [];
  constructor(private projectService: ProjectService, private router: Router,
              private taskService: TaskService, private userService: UserService) { }

  ngOnInit(): void {
    this.getProjects();
    this.getUsers();
  }
  getProjects()  {
    this.projectService.getProjects()
    .subscribe((data: Project[]) => this.projects = data);
   }

   getUsers()  {
    this.userService.getUsers()
    .subscribe((data: User[]) => this.users = data);
   }

  cancel(){
    this.saved = true;
    this.router.navigate(['/tasks']);
  }
  save(){
    this.task.projectId = +this.task.projectId; // parse value to number
    this.task.performerId = +this.task.performerId;
    this.taskService.createTask(this.task).subscribe();
    this.saved = true;
    this.router.navigate(['/tasks']);
  }
  canDeactivate(): boolean | Observable<boolean>{

      if (!this.saved){
          return confirm('Do you want to leave the page?');
      }
      else{
          return true;
      }
  }
}
