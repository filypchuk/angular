import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TasksListComponent } from './components/tasks-list/tasks-list.component';
import { TaskItemComponent } from './components/task-item/task-item.component';
import { TaskCreateComponent } from './components/task-create/task-create.component';
import { TaskUpdateComponent } from './components/task-update/task-update.component';
import { AppRoutingModule } from 'src/app/app-routing.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';



@NgModule({
  declarations: [TasksListComponent, TaskItemComponent, TaskCreateComponent, TaskUpdateComponent],
  imports: [CommonModule, AppRoutingModule, SharedModule, BrowserModule, FormsModule],
  exports: [TasksListComponent, TaskItemComponent, TaskCreateComponent, TaskUpdateComponent]
})
export class TasksModule { }
