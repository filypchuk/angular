import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserUpdateComponent } from './components/user-update/user-update.component';
import { UserCreateComponent } from './components/user-create/user-create.component';
import { UserItemComponent } from './components/user-item/user-item.component';
import { UsersListComponent } from './components/users-list/users-list.component';
import { AppRoutingModule } from 'src/app/app-routing.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';



@NgModule({
  declarations: [UserUpdateComponent, UserCreateComponent, UserItemComponent, UsersListComponent],
  exports: [UserUpdateComponent, UserCreateComponent, UserItemComponent, UsersListComponent],
  imports: [CommonModule, AppRoutingModule, SharedModule, BrowserModule, FormsModule]
})
export class UsersModule { }
