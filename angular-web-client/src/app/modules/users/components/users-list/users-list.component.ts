import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/models/user';
import { UserService } from 'src/app/services/user.service';
import { TeamService } from 'src/app/services/team.service';
import { forkJoin } from 'rxjs';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-users-list',
  templateUrl: './users-list.component.html',
  styleUrls: ['./users-list.component.css']
})
export class UsersListComponent implements OnInit {
  users: User[] = [];
  constructor(private userService: UserService, private teamService: TeamService) { }

  ngOnInit(): void {
    this.getUsers();
  }
  getUsers()  {
    forkJoin(this.userService.getUsers(), this.teamService.getTeams())
    .pipe(map(([ users, teams]) =>
    {
      users = users.map(user =>
      {
        user.team = teams.find(t => t.id === user.teamId );
        return user;
    });
      return users;
    })).subscribe(data => this.users = data);
   }
   deleteUser(userDelete: User) {
     const res = confirm('Are you sure, you want to delete user?');
     if (res) {
       // this.userService.deleteUser(userDelete.id).subscribe();
       this.users = this.users.filter(item => item.id !== userDelete.id);
     }
   }
}
