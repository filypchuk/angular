import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/models/user';
import { ComponentCanDeactivate } from 'src/app/shared/guards/exit.guard';
import { Team } from 'src/app/models/team';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from 'src/app/services/user.service';
import { TeamService } from 'src/app/services/team.service';
import { switchMap } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-user-update',
  templateUrl: './user-update.component.html',
  styleUrls: ['./user-update.component.css']
})
export class UserUpdateComponent implements OnInit, ComponentCanDeactivate {
  id: number;
  saved = false;
  user: User = { } as User;
  teams: Team[] = [];

  constructor(private userService: UserService, private router: Router, private route: ActivatedRoute,
              private teamService: TeamService) { }

  ngOnInit(): void {
    this.route.paramMap.pipe(
      switchMap(params => params.getAll('id')))
      .subscribe(data => this.id = +data);
    this.getUserById();
    this.getTeams();
  }
  getUserById()  {
    this.userService.getUserById(this.id)
    .subscribe((data: User) => this.user = data);
   }
  getTeams()  {
    this.teamService.getTeams()
     .subscribe((data: Team[]) => this.teams = data);
   }


  cancel(){
    this.saved = true;
    this.router.navigate(['/users']);
  }
  async save(){
    if (this.user.teamId) {
      this.user.teamId = +this.user.teamId;
    }

    this.userService.updateUser(this.user).subscribe();
    this.saved = true;
    this.router.navigate(['/users']);
  }
  canDeactivate(): boolean | Observable<boolean>{

    if (!this.saved){
        return confirm('Do you want to leave the page?');
    }
    else{
        return true;
    }
}
}
