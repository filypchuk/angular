import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProjectsListComponent } from './components/projects-list/projects-list.component';
import { ProjectUpdateComponent } from './components/project-update/project-update.component';
import { ProjectCreateComponent } from './components/project-create/project-create.component';
import { ProjectItemComponent } from './components/project-item/project-item.component';
import { AppRoutingModule } from 'src/app/app-routing.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';



@NgModule({
  declarations: [ProjectsListComponent,
    ProjectUpdateComponent, ProjectCreateComponent, ProjectItemComponent],
  imports: [CommonModule, AppRoutingModule, SharedModule, BrowserModule, FormsModule],
  exports: [ProjectsListComponent, ProjectUpdateComponent, ProjectCreateComponent]
})
export class ProjectsModule { }
