import { Component, OnInit, OnDestroy } from '@angular/core';
import { Project } from 'src/app/models/project';
import { ProjectService } from 'src/app/services/project.service';
import { takeUntil, map } from 'rxjs/operators';
import { UserService } from 'src/app/services/user.service';
import { forkJoin } from 'rxjs';
import { TeamService } from 'src/app/services/team.service';

@Component({
  selector: 'app-projects-list',
  templateUrl: './projects-list.component.html',
  styleUrls: ['./projects-list.component.css']
})
export class ProjectsListComponent implements OnInit {

  projects: Project [];

  constructor(private teamService: TeamService, private projectService: ProjectService, private userService: UserService) { }

  ngOnInit(): void {
    this.getProjects();
  }

  getProjects() {
    forkJoin(this.projectService.getProjects(), this.userService.getUsers(), this.teamService.getTeams())
    .pipe(map(([projects, users, teams]) =>
    {
      projects = projects.map(project =>
      {
        project.user = users.find(u => u.id === project.authorId );
        project.team = teams.find(t => t.id === project.teamId );
        return project;
    });
      return projects;
    })).subscribe(data => this.projects = data);
  }
  deleteProject(projectDelete: Project) {
    const res = confirm('Are you sure you want to delete project?');
    if (res) {
      this.projectService.deleteProject(projectDelete.id).subscribe();
      this.projects = this.projects.filter(item => item.id !== projectDelete.id);
    }
  }
}
