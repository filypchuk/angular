import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Project } from 'src/app/models/project';

@Component({
  selector: 'app-project-item',
  templateUrl: './project-item.component.html',
  styleUrls: ['./project-item.component.css']
})
export class ProjectItemComponent implements OnInit {

  @Input() project: Project;
  @Output() deletedProject = new EventEmitter<Project>();
  constructor() { }

  ngOnInit(): void {
  }

  delete() {
      this.deletedProject.emit(this.project);
  }
}
