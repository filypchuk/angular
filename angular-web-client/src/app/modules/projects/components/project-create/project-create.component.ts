import { Component, OnInit } from '@angular/core';
import { Project } from 'src/app/models/project';
import { Router } from '@angular/router';
import { ProjectService } from 'src/app/services/project.service';
import { Observable } from 'rxjs';
import { Team } from 'src/app/models/team';
import { TeamService } from 'src/app/services/team.service';
import { User } from 'src/app/models/user';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-project-create',
  templateUrl: './project-create.component.html',
  styleUrls: ['./project-create.component.css']
})
export class ProjectCreateComponent implements OnInit {
  saved = false;
  project: Project = { } as Project;
  teams: Team[] = [];
  users: User[] = [];
  constructor(private projectService: ProjectService, private router: Router,
              private teamService: TeamService, private userService: UserService) { }

  ngOnInit(): void {
    this.getTeams();
    this.getUsers();
  }
  getTeams()  {
    this.teamService.getTeams()
    .subscribe((data: Team[]) => this.teams = data);
   }

   getUsers()  {
    this.userService.getUsers()
    .subscribe((data: User[]) => this.users = data);
   }

  cancel(){
    this.saved = true;
    this.router.navigate(['/projects']);
  }
  save(){
    this.project.teamId = +this.project.teamId; // parse value to number
    this.project.authorId = +this.project.authorId;
    this.projectService.createProject(this.project).subscribe();
    this.saved = true;
    this.router.navigate(['/projects']);
  }
  canDeactivate(): boolean | Observable<boolean>{

      if (!this.saved){
          return confirm('Do you want to leave the page?');
      }
      else{
          return true;
      }
  }

}
