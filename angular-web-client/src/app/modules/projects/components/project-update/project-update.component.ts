import { Component, OnInit } from '@angular/core';
import { ComponentCanDeactivate } from 'src/app/shared/guards/exit.guard';
import { Observable } from 'rxjs';
import { Project } from 'src/app/models/project';
import { ProjectService } from 'src/app/services/project.service';
import { ActivatedRoute, Router } from '@angular/router';
import { switchMap} from 'rxjs/operators';
import { TeamService } from 'src/app/services/team.service';
import { UserService } from 'src/app/services/user.service';
import { Team } from 'src/app/models/team';
import { User } from 'src/app/models/user';

@Component({
  selector: 'app-project-update',
  templateUrl: './project-update.component.html',
  styleUrls: ['./project-update.component.css']
})

export class ProjectUpdateComponent implements OnInit, ComponentCanDeactivate {
  id: number;
  saved = false;
  project: Project = {} as Project;
  teams: Team[] = [];
  users: User[] = [];

  constructor(private route: ActivatedRoute, private projectService: ProjectService, private router: Router,
              private teamService: TeamService, private userService: UserService) { }

  ngOnInit(): void {
    this.route.paramMap.pipe(
      switchMap(params => params.getAll('id')))
      .subscribe(data => this.id = +data);
    this.getProjectById();
    this.getTeams();
    this.getUsers();
  }

  getTeams()  {
    this.teamService.getTeams()
    .subscribe((data: Team[]) => this.teams = data);
   }

   getUsers()  {
    this.userService.getUsers()
    .subscribe((data: User[]) => this.users = data);
   }
  getProjectById()  {
   this.projectService.getProjectById(this.id)
    .subscribe((data: Project) => this.project = data);
  }
  cancel(){
    this.saved = true;
    this.router.navigate(['/projects']);
  }
  async save(){
    this.project.teamId = +this.project.teamId; // parse value to number
    this.project.authorId = +this.project.authorId;

    this.projectService.updateProject(this.project).subscribe();
    this.saved = true;
    this.router.navigate(['/projects']);
  }
  canDeactivate(): boolean | Observable<boolean>{

      if (!this.saved){
          return confirm('Do you want to leave the page?');
      }
      else{
          return true;
      }
  }
}
