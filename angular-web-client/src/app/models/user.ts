import { Team } from './team';

export interface User {
    id: number;
    firstName: string;
    lastName: string;
    email: string;
    birthday: Date;
    registeredAt: Date;
    teamId?: number;
    team: Team;
}
