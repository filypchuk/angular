import { TaskStates } from './task-states';
import { Project } from './project';
import { User } from './user';

export interface Task {
    id: number;
    name: string;
    description: string;
    createdAt: Date;
    finishedAt: Date;
    state: TaskStates;
    projectId: number;
    project: Project;
    performerId?: number;
    user: User;
}
