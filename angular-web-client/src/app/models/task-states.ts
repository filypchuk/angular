export enum TaskStates {
    Created = 0,
    Started,
    Finished,
    Canceled
}
