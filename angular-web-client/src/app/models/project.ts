import { User } from './user';
import { Team } from './team';

export interface Project {
    id: number;
    name: string;
    description: string;
    createdAt: Date;
    deadline: Date;
    authorId?: number;
    teamId?: number;
    user: User;
    team: Team;
}
