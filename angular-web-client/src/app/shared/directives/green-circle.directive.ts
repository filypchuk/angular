import {Directive, ElementRef, Renderer2, Input, HostBinding, OnInit} from '@angular/core';

@Directive({
  selector: '[appGreenCircle]'
})

export class GreenCircleDirective implements OnInit {

  @Input() colorCircle = '#bbb';
  private color: string;
  constructor(private elementRef: ElementRef, private renderer: Renderer2) {

    this.renderer.setStyle(this.elementRef.nativeElement, 'height', '1em');
    this.renderer.setStyle(this.elementRef.nativeElement, 'width', '1em');
    this.renderer.setStyle(this.elementRef.nativeElement, 'border-radius', '50%');
    this.renderer.setStyle(this.elementRef.nativeElement, 'display', 'inline-block');
    this.renderer.setStyle(this.elementRef.nativeElement, 'vertical-align', 'middle');
  }


  ngOnInit() {
      this.color = this.colorCircle;
  }
  @HostBinding('style.background-color') get getFontSize(){

    return this.color;
}
}
