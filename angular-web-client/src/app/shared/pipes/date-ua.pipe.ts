import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'dateUa'
})
export class DateUaPipe implements PipeTransform {

  transform(value: Date, ...args: any[]): string {
    const options = { year: 'numeric', month: 'long', day: 'numeric' };
    const date = new Date(value);
    return date.toLocaleDateString('uk-UA', options);
  }

}
