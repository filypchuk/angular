import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavigationMenuComponent } from './components/navigation-menu/navigation-menu.component';
import { GreenCircleDirective } from './directives/green-circle.directive';
import { AppRoutingModule } from '../app-routing.module';
import { DateUaPipe } from './pipes/date-ua.pipe';
import { RedCircleDirective } from './directives/red-circle.directive';



@NgModule({
  declarations: [NavigationMenuComponent, GreenCircleDirective, DateUaPipe, RedCircleDirective],
  imports: [
    CommonModule, AppRoutingModule
  ],
  exports: [NavigationMenuComponent, GreenCircleDirective, DateUaPipe]
})
export class SharedModule { }
