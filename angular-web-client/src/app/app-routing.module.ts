import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProjectsListComponent } from './modules/projects/components/projects-list/projects-list.component';
import { TasksListComponent } from './modules/tasks/components/tasks-list/tasks-list.component';
import { ProjectUpdateComponent } from './modules/projects/components/project-update/project-update.component';
import { ProjectCreateComponent } from './modules/projects/components/project-create/project-create.component';
import { ExitGuard } from './shared/guards/exit.guard';
import { TaskCreateComponent } from './modules/tasks/components/task-create/task-create.component';
import { TeamsListComponent } from './modules/teams/components/teams-list/teams-list.component';
import { TeamCreateComponent } from './modules/teams/components/team-create/team-create.component';
import { TaskUpdateComponent } from './modules/tasks/components/task-update/task-update.component';
import { TeamUpdateComponent } from './modules/teams/components/team-update/team-update.component';
import { UsersListComponent } from './modules/users/components/users-list/users-list.component';
import { UserCreateComponent } from './modules/users/components/user-create/user-create.component';
import { UserUpdateComponent } from './modules/users/components/user-update/user-update.component';


const routes: Routes = [
{ path: '', component: ProjectsListComponent},
{ path: 'projects', component: ProjectsListComponent},
{ path: 'projects/create', component: ProjectCreateComponent, canDeactivate: [ExitGuard]},
{ path: 'projects/:id/update', component: ProjectUpdateComponent, canDeactivate: [ExitGuard]},
{ path: 'tasks', component: TasksListComponent},
{ path: 'tasks/create', component: TaskCreateComponent, canDeactivate: [ExitGuard]},
{ path: 'tasks/:id/update', component: TaskUpdateComponent, canDeactivate: [ExitGuard]},
{ path: 'teams', component: TeamsListComponent},
{ path: 'teams/create', component: TeamCreateComponent, canDeactivate: [ExitGuard]},
{ path: 'teams/:id/update', component: TeamUpdateComponent, canDeactivate: [ExitGuard]},
{ path: 'users', component: UsersListComponent},
{ path: 'users/create', component: UserCreateComponent, canDeactivate: [ExitGuard]},
{ path: 'users/:id/update', component: UserUpdateComponent, canDeactivate: [ExitGuard]},
{ path: '**', redirectTo: '/'}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
