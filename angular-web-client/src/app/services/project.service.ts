import { Injectable } from '@angular/core';
import { HttpInternalService } from './http-internal.service';
import { Project } from '../models/project';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProjectService {
  public routePrefix = '/api/projects';

  constructor(private httpService: HttpInternalService) { }

  public getProjects(): Observable<Project[]> {
    return this.httpService.getRequest<Project[]>(`${this.routePrefix}`);
  }

  public getProjectById(id: number): Observable<Project> {
    return this.httpService.getRequest<Project>(`${this.routePrefix}/${id}`);
  }

  public createProject(project: Project) {
    return this.httpService.postRequest<Project>(`${this.routePrefix}`, project);
  }

  public updateProject(project: Project) {
    return this.httpService.putRequest<Project>(`${this.routePrefix}`, project);
  }

  public deleteProject(id: number) {
    return this.httpService.deleteRequest<Project>(`${this.routePrefix}/${id}`);
  }
}
