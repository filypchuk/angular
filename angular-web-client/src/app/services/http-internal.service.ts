import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class HttpInternalService {
  public baseUrl: string = environment.apiUrl;

  constructor(private http: HttpClient) { }

  public getRequest<T>(url: string): Observable<T> {
    console.log(this.baseUrl + url);
    return this.http.get<T>(this.baseUrl + url);
  }

  public postRequest<T>(url: string, createEntity: object): Observable<T> {
    return this.http.post<T>(this.baseUrl + url, createEntity);
  }

  public putRequest<T>(url: string, updateEntity: object): Observable<T> {
      return this.http.put<T>(this.baseUrl + url, updateEntity);
  }

  public deleteRequest<T>(url: string): Observable<T> {
      return this.http.delete<T>(this.baseUrl + url);
  }
}
