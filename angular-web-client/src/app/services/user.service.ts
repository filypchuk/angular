import { Injectable } from '@angular/core';
import { User } from '../models/user';
import { HttpInternalService } from './http-internal.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  public routePrefix = '/api/users';

  constructor(private httpService: HttpInternalService) { }

  public getUsers(): Observable<User[]> {
    return this.httpService.getRequest<User[]>(`${this.routePrefix}`);
  }

  public getUserById(id: number): Observable<User> {
    return this.httpService.getRequest<User>(`${this.routePrefix}/${id}`);
  }

  public createUser(user: User) {
    return this.httpService.postRequest<User>(`${this.routePrefix}`, user);
  }

  public updateUser(user: User) {
    return this.httpService.putRequest<User>(`${this.routePrefix}`, user);
  }

  public deleteUser(id: number) {
    return this.httpService.deleteRequest<User>(`${this.routePrefix}/${id}`);
  }
}
