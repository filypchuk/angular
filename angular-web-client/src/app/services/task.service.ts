import { Injectable } from '@angular/core';
import { HttpInternalService } from './http-internal.service';
import { Task } from '../models/task';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TaskService {
  public routePrefix = '/api/tasks';

  constructor(private httpService: HttpInternalService) { }

  public getTasks(): Observable<Task[]> {
    return this.httpService.getRequest<Task[]>(`${this.routePrefix}`);
  }

  public getTaskById(id: number): Observable<Task> {
    return this.httpService.getRequest<Task>(`${this.routePrefix}/${id}`);
  }

  public createTask(task: Task) {
    return this.httpService.postRequest<Task>(`${this.routePrefix}`, task);
  }

  public updateTask(task: Task) {
    return this.httpService.putRequest<Task>(`${this.routePrefix}`, task);
  }

  public deleteTask(id: number) {
    return this.httpService.deleteRequest<Task>(`${this.routePrefix}/${id}`);
  }
}
