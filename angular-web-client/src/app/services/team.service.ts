import { Injectable } from '@angular/core';
import { HttpInternalService } from './http-internal.service';
import { Observable } from 'rxjs';
import { Team } from '../models/team';

@Injectable({
  providedIn: 'root'
})
export class TeamService {
  public routePrefix = '/api/teams';

  constructor(private httpService: HttpInternalService) { }

  public getTeams(): Observable<Team[]> {
    return this.httpService.getRequest<Team[]>(`${this.routePrefix}`);
  }

  public getTeamById(id: number): Observable<Team> {
    return this.httpService.getRequest<Team>(`${this.routePrefix}/${id}`);
  }

  public createTeam(team: Team) {
    return this.httpService.postRequest<Team>(`${this.routePrefix}`, team);
  }

  public updateTeam(team: Team) {
    return this.httpService.putRequest<Team>(`${this.routePrefix}`, team);
  }

  public deleteTeam(id: number) {
    return this.httpService.deleteRequest<Team>(`${this.routePrefix}/${id}`);
  }
}
